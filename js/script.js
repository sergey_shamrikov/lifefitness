$(document).ready(function(){

  // nav
  function navPosition(){
    var windowScroll = $(window).scrollTop(),
        offset = $("#nav").offset().top;
    if(windowScroll>=offset){
      $('.main_menu').addClass('fixed_nav');
    }
    else{
      $('.main_menu').removeClass('fixed_nav');
    }
  }
  navPosition();
  $(window).on('scroll',navPosition);


  // product gallery
  $(".product_tumbs_list_box").jCarouselLite({
    btnNext: ".product_tumbs_btn_next",
    btnPrev: ".product_tumbs_btn_prev",
    mouseWheel: true,
    circular: false, //loop
    vertical: true
  });

  function firstSlide () {
    var src = $('.product_tumbs_list>li:first-child .product_tumbs_item').attr('data-src'),
        firstSlide = $('.product_tumbs_list>li:first-child .product_tumbs_item').attr('data-lightbox');
    $('.product_gallery_img>a').attr('href',firstSlide);
    $('.product_gallery_img>a img').attr('src',src);
    $('.product_tumbs_list>li:first-child .product_tumbs_item').addClass('active');
  }
  firstSlide ();
  $('.product_tumbs_item').on('click',function(){
    var $this = $(this),
        src = $this.attr("data-src"),
        href = $this.attr("data-lightbox"),
        img = $this.parents('.product_gallery').find('.product_gallery_img img'),
        link = $this.parents('.product_gallery').find('.product_gallery_img a');
    $(img).attr('src',src);
    $(link).attr('href',href);
    $this.addClass('active').parent().siblings().find('.product_tumbs_item').removeClass('active');
  });

  $('.product_gallery_img a').fancybox()


  // tabs
  $('.tabs_list_item').on('click',function(){
    var index = $(this).index();
    $(this).addClass('active').siblings().removeClass('active');
    $(this).parents(".tabs_box").find('.tabs_item').eq(index).addClass('active').siblings().removeClass('active');
  });

  // modal window
  $(".popup_open").on('click',function(){
    var modal = $(this).attr('data-modal');
    $(modal).arcticmodal();
  });

  // main_slider
  $('#main_slider').owlCarousel({
 
      // navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });


});